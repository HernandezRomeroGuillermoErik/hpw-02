Alumno =[
    
    {"num_control": "10160838",
    "nom_completo" : "Hernandez Romero Guillermo Erik",
    "grupo": "A"},
    
    {"num_control": "10160839",
    "nom_completo" : "Perez Cruz Gabriela",
    "grupo": "A"},
    
    {"num_control": "10160840",
    "nom_completo" : "Gomez Santos Rolando",
    "grupo": "A"},


];

Materias =[
  {"clave" : "01",
   "nombre": "Matematicas"},
   
   {"clave" : "02",
   "nombre": "Historia"},
   
   {"clave" : "03",
   "nombre": "Geografia"},
   
   {"clave" : "04",
   "nombre": "Etica"},
   
   {"clave" : "05",
   "nombre": "Biologia"},
   
   {"clave" : "06",
   "nombre": "Quimica"},
   
   {"clave" : "07",
   "nombre": "Fisica"},
];

Profesores =[
{"clave":"101",
 "nombre": "Perez Laguna Miguel"},
 
{"clave":"102",
 "nombre": "Lopez Colmenares Francisco"},
 
{"clave":"103",
 "nombre": "Guzman Perez Juana"}, 
 
{"clave":"104",
 "nombre": "Torres Mijanjos Pablo"},
    
];

Horarios =[
 {"hora_inicial":"7:00 AM",
   "hora_final":"8:00 AM",
   "clave_mat":"01",
   "clave_prof":"101",
},

 {"hora_inicial":"8:00 AM",
   "hora_final":"9:00 AM",
   "clave_mat":"02",
   "clave_prof":"102",
},

 {"hora_inicial":"10:00 AM",
   "hora_final":"11:00 AM",
   "clave_mat":"03",
   "clave_prof":"103",
},

 {"hora_inicial":"11:00 AM",
   "hora_final":"12:00 PM",
   "clave_mat":"04",
   "clave_prof":"104",
},

 {"hora_inicial":"12:00 PM",
   "hora_final":"01:00 PM",
   "clave_mat":"05",
   "clave_prof":"101",
},

 {"hora_inicial":"1:00 PM",
   "hora_final":"2:00 PM",
   "clave_mat":"06",
   "clave_prof":"102",
},

 {"hora_inicial":"2:00 PM",
   "hora_final":"3:00 OM",
   "clave_mat":"07",
   "clave_prof":"103",
},

];

Grupo={
   "clave": "A",
   "nombre": "ISA",
   "alumno":Alumno,
   "materias":Materias,
   "Profesores": Profesores,
   "Horarios": Horarios
 };
 
 /*Crear una funcion que dado un grupo, imprima una lista de alumnos*/

 function imprimir_lista_alumnos(grupo){
     
     for(var i=0;i<grupo["alumno"].length;i++){
                console.log(grupo["alumno"][i]["nom_completo"]);
       }
   
 };
 
 console.log(imprimir_lista_alumnos(Grupo));
 
/*Crear una funcion que dado un grupo, imprima las relaciones. Horario-Profesor-Materia*/

function imprimir_lista_horarios(grupo){
      
      for(var i=0;i<grupo["Horarios"].length;i++){
         
       }

}

console.log(imprimir_lista_horarios(Grupo));

/*Funciones de Busqueda dentro de un Grupo un Alumno, Profesor y Materia*/

/*Busqueda Alumno*/
function busqueda_alumno(grupo,numcontrol){
         var estado=false;
         for(var i=0;i<grupo["alumno"].length;i++){
                if(numcontrol==grupo["alumno"][i]["num_control"]){
                    estado=true;
                }
       }
       return estado;
    }
    
console.log(busqueda_alumno(Grupo,"10160838"))

/*Busqueda Profesor*/
function busqueda_profesor(grupo,clave){
         var estado=false;
         for(var i=0;i<grupo["Profesores"].length;i++){
                if(clave==grupo["Profesores"][i]["clave"]){
                    estado=true;
                }
       }
       return estado;
    }
    
console.log(busqueda_profesor(Grupo,"101"))

/*Busqueda Materia*/
function busqueda_materia(grupo,clave){
         var estado=false;
         for(var i=0;i<grupo["materias"].length;i++){
                if(clave==grupo["materias"][i]["clave"]){
                    estado=true;
                }
       }
       return estado;
    }
    
console.log(busqueda_materia(Grupo,"01"))

/*Operación CRUD*/
/*Insertar Alumno (Verificar que no se repita el numero de control),
Profesor (Verificar que no se repita su clave), Materia (Verificar que no se repita su clave)*/

function inser_alumno(grupos,numcontrol,nombre,grupo){
         var estado;
         if(busqueda_alumno(grupos,numcontrol)==false){        
                     Alumno.push({"num_control": numcontrol,
                     "nom_completo" : nombre,
                     "grupo": grupo});
                     var estado=true;
         }
         else{
             var estado=false;
         }
      return estado;
}

console.log(inser_alumno(Grupo,"10160841","Gomez Santos Rolando","A"));

function inser_profe(grupos,clave,nombre){
         var estado;
         if(busqueda_profesor(grupos,clave)==false){        
                     Profesores.push({"clave": clave,
                     "nombre" : nombre});
                     var estado=true;
         }
         else{
             var estado=false;
         }
      return estado;
}

console.log(inser_profe(Grupo,"108","Memo"));

function inser_mate(grupos,clave,materia){
         var estado;
         if(busqueda_materia(grupos,clave)==false){        
                     Materias.push({"clave": clave,
                     "nombre" : materia});
                     var estado=true;
         }
         else{
             var estado=false;
         }
      return estado;
}

console.log(inser_mate(Grupo,"08","Educacion Fisica"));


function elim_alumno(grupos,numcontrol,nombre,grupo){
         var estado;
         if(busqueda_alumno(grupos,numcontrol)==false){        
                     Alumno.push({"num_control": numcontrol,
                     "nom_completo" : nombre,
                     "grupo": grupo});
                     var estado=true;
         }
         else{
             var estado=false;
         }
      return estado;
}

console.log(inser_alumno(Grupo,"10160841","Gomez Santos Rolando","A"));

/*Eliminar*/


